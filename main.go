package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"net/netip"
	"os"
	"slices"
	"sync"
	"sync/atomic"
	"time"
)

func logErr(msg error) {
	log.Printf("ERROR: %s", msg)
}

func logErrf(format string, args ...any) {
	logErr(fmt.Errorf(format, args...))
}

type handlerFunc func(*net.TCPConn) error

type Upstream struct {
	Name  string
	Addr  string
	Match [][]byte
}

func parseAddr(addr string) (net.TCPAddr, error) {
	ap, err := netip.ParseAddrPort(addr)
	if err != nil {
		return net.TCPAddr{}, fmt.Errorf("Error parsing address %q: %w", addr, err)
	}
	tAddr := net.TCPAddrFromAddrPort(ap)
	if tAddr == nil {
		return net.TCPAddr{}, fmt.Errorf("Invalid TCP address %q", addr)
	}
	return *tAddr, nil
}

func NewUpstream(name, addr string, match [][]byte) (*Upstream, error) {
	return &Upstream{name, addr, match}, nil
}

type Upstreams struct {
	upstreams      []Upstream
	length         int
	hasBlank       bool
	pool           sync.Pool
	resolver       *net.Resolver
	resolveTimeout time.Duration
}

func NewUpstreams() *Upstreams {
	var u Upstreams
	u = Upstreams{
		resolver: &net.Resolver{PreferGo: true},
		pool: sync.Pool{
			New: func() any {
				return make([]byte, u.length)
			},
		},
	}
	return &u
}

func NewUpstreamsFromFile(filename string) (*Upstreams, error) {
	result := NewUpstreams()
	type upstreamConfig struct {
		Name  string   `json:"name"`
		Addr  string   `json:"addr"`
		Match []string `json:"match"`
	}
	c := []upstreamConfig{}
	f, err := os.Open(filename)
	if err != nil {
		return result, fmt.Errorf("Error opening file %q: %w", filename, err)
	}
	if err := json.NewDecoder(f).Decode(&c); err != nil {
		return result, fmt.Errorf("Error decoding file %q: %w", filename, err)
	}
	for _, upstream := range c {
		log.Printf("Adding upstream %q", upstream.Name)
		match := make([][]byte, len(upstream.Match))
		for i, m := range upstream.Match {
			match[i] = ([]byte)(m)
		}
		if err := result.Add(upstream.Name, upstream.Addr, match); err != nil {
			return result, fmt.Errorf("Error adding upstream %q: %w", upstream.Name, err)
		}
	}
	return result, nil
}

func (u *Upstreams) Add(name, addr string, match [][]byte) error {
	if u.hasBlank {
		return fmt.Errorf("Cannot add upstream after one with a blank match has been added")
	}
	upstream, err := NewUpstream(name, addr, match)
	if err != nil {
		return fmt.Errorf("Error creating upstream: %w", err)
	}
	u.upstreams = append(u.upstreams, *upstream)
	if len(match) == 0 {
		u.hasBlank = true
	}
	for _, item := range match {
		if len(item) > u.length {
			u.length = len(item)
		}
	}
	return nil
}

func (u *Upstreams) MustAdd(name, addr string, match [][]byte) {
	err := u.Add(name, addr, match)
	if err != nil {
		log.Fatalf("Error adding upstream: %s", err)
	}
}

func (u *Upstreams) Match(buf []byte) (Upstream, int, error) {
	if len(buf) != u.length {
		panic("Tried to call match with an incorrect length buffer")
	}
	for _, upstream := range u.upstreams {
		if len(upstream.Match) == 0 {
			return upstream, 0, nil
		}
		for _, match := range upstream.Match {
			if slices.Equal(match, buf[0:len(match)]) {
				return upstream, len(match), nil
			}
		}
	}
	return Upstream{}, 0, fmt.Errorf("No matching upstream found")
}

func (u *Upstreams) Proxy(conn *net.TCPConn) error {
	uconn, upstream, err := func() (*net.TCPConn, Upstream, error) {
		buf := u.pool.Get().([]byte)
		// We wrap this in an anonymous function so that this deferred Put() can be
		// called as soon as we're done with the buffer
		defer u.pool.Put(buf)
		n, err := conn.Read(buf)
		if err != nil {
			return nil, Upstream{}, fmt.Errorf("Error reading from client: %w", err)
		} else if n != len(buf) {
			return nil, Upstream{}, fmt.Errorf("Partial Read: Reading %d bytes from client returned %d bytes", len(buf), n)
		}
		upstream, length, err := u.Match(buf)
		if err != nil {
			return nil, Upstream{}, fmt.Errorf("Error matching connection: %w", err)
		}
		log.Printf("OPEN  %s: %s -> %s", upstream.Name, conn.RemoteAddr().String(), upstream.Addr)
		uconn, err := upstream.InitialTransfer(buf, length)
		return uconn, upstream, err
	}()
	if err != nil {
		log.Printf("ERROR %s: %s -> %s", upstream.Name, conn.RemoteAddr().String(), upstream.Addr)
		return err
	}
	defer log.Printf("CLOSE %s: %s -> %s", upstream.Name, conn.RemoteAddr().String(), upstream.Addr)
	return Proxy(conn, uconn)
}

func (u Upstream) InitialTransfer(buf []byte, length int) (*net.TCPConn, error) {
	addr, err := net.ResolveTCPAddr("tcp", u.Addr)
	if err != nil {
		return nil, fmt.Errorf("Error resolving address %q: %w", u.Addr, err)
	}
	uconn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		return nil, fmt.Errorf("Error connecting to address %q (resolved from %q): %w", addr.String(), u.Addr, err)
	}
	n, err := uconn.Write(buf)
	if err != nil {
		return nil, fmt.Errorf("Error writing initial bytes to server %q: %w", u.Addr, err)
	} else if n != len(buf) {
		return nil, fmt.Errorf("Partial Write: Writing %d bytes to server %q only wrote %d bytes", len(buf), u.Addr, n)
	}
	return uconn, nil
}

func Proxy(a, b *net.TCPConn) error {
	err := func() error {
		wg := sync.WaitGroup{}
		wg.Add(2)
		err := atomic.Value{}
		go func() {
			defer wg.Done()
			_, e := io.Copy(a, b)
			if e != nil {
				err.Store(e)
			}
			a.CloseWrite()
		}()
		go func() {
			defer wg.Done()
			_, e := io.Copy(b, a)
			if e != nil {
				err.Store(e)
			}
			b.CloseWrite()
		}()
		wg.Wait()
		e := err.Load()
		if e == nil {
			return nil
		} else {
			return e.(error)
		}
	}()
	if err != nil {
		return fmt.Errorf("Error proxying connection: %w", err)
	}
	return nil
}

func makeHandler(upstreams *Upstreams) handlerFunc {
	return func(conn *net.TCPConn) error {
		defer conn.Close()
		return upstreams.Proxy(conn)
	}
}

func run(addr string, handler handlerFunc) error {
	tAddr, err := parseAddr(addr)
	if err != nil {
		return fmt.Errorf("Error prasing listen address: %w", err)
	}
	server, err := net.ListenTCP("tcp", &tAddr)
	if err != nil {
		return fmt.Errorf("Error listening on TCP %q: %w", tAddr.String(), err)
	}
	fmt.Printf("Listening on TCP %q\n", tAddr.String())
	for {
		conn, err := server.AcceptTCP()
		if err != nil {
			logErrf("Error accepting connection: %w", err)
		}
		go func(conn *net.TCPConn) {
			if err := handler(conn); err != nil {
				logErrf("Error handling connection from %q: %w", conn.RemoteAddr().String(), err)
			}
		}(conn)
	}
}

func main() {
	if len(os.Args) != 3 {
		fmt.Printf("Usage: %s <LISTEN_ADDR> <UPSTREAMS_FILE>\n", os.Args[0])
		os.Exit(1)
	}
	addr := os.Args[1]
	file := os.Args[2]
	upstreams, err := NewUpstreamsFromFile(file)
	if err != nil {
		log.Fatal(err)
	}
	if err := run(addr, makeHandler(upstreams)); err != nil {
		log.Fatalf("Existing with error: %s", err)
	}
}
