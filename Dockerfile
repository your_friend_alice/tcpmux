FROM golang:1.21-alpine AS builder
RUN mkdir /build \
    && apk add --no-cache libcap
WORKDIR /build
COPY go.mod main.go /build
RUN go build -ldflags "-s -w" .

FROM alpine:latest AS setcap
RUN apk add --no-cache libcap
COPY --from=builder /build/tcpmux /
RUN setcap cap_net_bind_service=ep /tcpmux

FROM scratch
COPY --from=setcap /tcpmux /
COPY upstreams.json /config/upstreams.json
ENTRYPOINT ["/tcpmux"]
CMD ["0.0.0.0:443", "/config/upstreams.json"]
