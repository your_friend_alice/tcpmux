# tcpmux

A "TCP multiplexer". Forward TCP connections to a port to different upstream addresses, based on the first few bytes the client sends.

The primary use case is to allow you to seamlessly serve both HTTPS and SSH traffic on the same port. This is possible because according to the SSH protocol, the first 3 bytes the client sends will always be "SSH", while a TLS connection's ClientHello message can start with a variety of byte sequences, none of them being equal to "SSH".

The main use-case is for sysadmins on the go (and on a budget), as a lot of public wifi, corporate networks, and other firewall setups have the annoying habit of blocking traffic on all TCP ports except 80 and 443. As the multiplexing transparently happens on a per-connection basis, you can make HTTPS and SSH connections to the same server, on the same port, at the same time, and it will Just Work.

**This is not an implementation of the deprecated [TCPMUX](https://en.wikipedia.org/wiki/TCP_Port_Service_Multiplexer) protocol, the name is just too concise to pass up on.** When configured correctly (such as with the example configuration in this repo), this transparently forwards connections based on their protocol, with no additional requirements for the client.

## Usage

```
Usage: ./tcpmux <LISTEN_ADDR> <UPSTREAMS_FILE>
```

`<LISTEN_ADDR>` is the address tcpmux will listen on for incoming connections. This will be parsed by [netip.ParseAddressPort()](https://pkg.go.dev/net/netip#ParseAddrPort), and takes the form `address:port`. You can give the address as `0.0.0.0` to listen on all addresses.

`<UPSTREAMS_FILE>` refers to a simple JSON file, containing a list of objects with keys:

| Name | Type | Description |
|------|------|-------------|
| name | `string` | The name of the upstream protocol, just used for logging |
| addr | `string` | The address to foward the connection to, parsed by [net.ParseTCPAddr()](https://pkg.go.dev/net#ResolveTCPAddr) |
| match | `list(string)` | List of byte sequences to match. The first N bytes the client sends will be compared to these strings in order (with N = the length of the list entry being checked). An empty list matches everything. |

The entries in the JSON list will be checked in the order they are listed in the file. If the file has an entry that matches everything, it cannot have any entries after it as they would never match.

If you want to use this to multiplex HTTPS and SSH on a single port, you can use the provided `upstreams.json` in this repo.

## Requirements

- [Go 1.21+](https://go.dev/doc/install)

## Installation

```
go install gitlab.com/your_friend_alice/tcpmux@latest
```

If you want to listen on a low port number, running this as root would be overkill. You can grant `CAP_NET_BIND_SERVICE` to the binary with `sudo setcap 'cap_net_bind_service=ep' "$(which tcpmux)"`. Alternately, if you want to deploy this as a systemd service, you can add the capability in the unit file. See `tcpmux.example.service` for an example unit file.

## Building Locally

```
go build -ldflags "-s -w" .
```

Note that the `ldflags` arguments are not needed, this just reduce the final size of the binary.

This repo also includes a Dockerfile to build a very minimal Docker image. The built Docker image includes the default `upstreams.json` in a directory named `/config`, and the default CMD references this file; you can supply your own configuration by mounting a volume at `/config` containing a custom `upstreams.json`.

## Example

Here, an HTTPS server is listening on port 8443, and an SSH server is listening on port 8422.

```
cat <<EOF > upstreams.json
[
  {
    "name": "ssh",
    "addr": "127.0.0.1:22",
    "match": [
      "SSH"
    ]
  },
  {
    "name": "https",
    "addr": "127.0.0.1:443",
    "match": []
  }
]
EOF
tcpmux 0.0.0.0:443 upstreams.json
```

In this example, the machine is receiving traffic on a network interface with the address `1.2.3.4`, and has an HTTPS server listening on `127.0.0.1:443`, and an SSH server is listening on port `0.0.0.0:22`. Even though `tcpmux` and the HTTPS server are both listening on port 443, they are able to do so because they are listening on different addresses.

```
cat <<EOF > upstreams.json
[
  {
    "name": "ssh",
    "addr": "127.0.0.1:22",
    "match": [
      "SSH"
    ]
  },
  {
    "name": "https",
    "addr": "127.0.0.1:443",
    "match": []
  }
]
EOF
tcpmux 1.2.3.4:443 upstreams.json
```
